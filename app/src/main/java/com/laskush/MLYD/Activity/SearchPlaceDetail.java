package com.laskush.MLYD.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.laskush.MLYD.R;


public class SearchPlaceDetail extends AppCompatActivity {
ImageButton map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_place_detail);
        map=(ImageButton)findViewById(R.id.google_maps);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String geoCode = "geo:0,0?q=" + 27.7214093 + "," + 85.361921 + "";
                Intent sendLocationToMap = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(geoCode));
                startActivity(sendLocationToMap);


            }
        });

    }


}
