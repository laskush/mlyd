package com.laskush.MLYD.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.laskush.MLYD.R;
import com.laskush.MLYD.adapter.PlaceAdapter;

public class SearchPlace extends Fragment {
    EditText search;
    ListView placeList;
   String[] title = {
            "BoudhaNath","Pashupatinath","Gokarna"


    };
    Integer[] imageId = {
            R.drawable.addphoto,


    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.activity_search_place, container, false);
//        actionBar.show();
        placeList = (ListView)content. findViewById(R.id.placeList);
        PlaceAdapter adapter = new PlaceAdapter(getActivity(), title, imageId);
        placeList.setAdapter(adapter);
        placeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String selectedFromList = (placeList.getItemAtPosition(position).toString());
                if (selectedFromList.equals("BoudhaNath")) {

                    Intent i = new Intent(getActivity(), SearchPlaceDetail.class);
                    startActivity(i);
                }
                if (selectedFromList.equals("Pashupatinath"))
                    Toast.makeText(getActivity(),"PashupatiNath",Toast.LENGTH_SHORT).show();
                if (selectedFromList.equals("Gokarna"))
                    Toast.makeText(getActivity(),"Gokarna",Toast.LENGTH_SHORT).show();

            }
        });
        return  content;

    }


}


