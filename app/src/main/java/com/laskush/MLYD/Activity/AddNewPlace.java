package com.laskush.MLYD.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.laskush.MLYD.R;
import com.laskush.MLYD.Utils.PrefUtils;


public class AddNewPlace extends Fragment{
    Button btn_login;
    Button btn_signup;
    EditText username,password;
    String name,pass;

    //UserSessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.activity_add_new_place, container, false);
        btn_login = (Button) content.findViewById(R.id.login);
        btn_signup = (Button) content.findViewById(R.id.signup);
        username = (EditText) content.findViewById(R.id.username);
        password = (EditText) content.findViewById(R.id.passwrd);





        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                name= username.getText().toString();
                pass= password.getText().toString();

                if (name.equals(PrefUtils.Ret_Name(getActivity())) && pass.equals(PrefUtils.Ret_Password(getActivity()))) {

                    Intent i = new Intent(getActivity(), AddPlaceDetail.class);
                    startActivity(i);
                }
                else {
                    Toast.makeText(getActivity(),"Login Failure",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SignUp.class);
                startActivity(i);

            }
        });

        return content;
      }
}
