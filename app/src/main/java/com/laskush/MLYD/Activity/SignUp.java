package com.laskush.MLYD.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.laskush.MLYD.MainActivity;
import com.laskush.MLYD.R;
import com.laskush.MLYD.Utils.PrefUtils;

public class SignUp extends AppCompatActivity {
    Button create;
    EditText add_name,add_passwrd,add_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        create=(Button)findViewById(R.id.create);
        add_name = (EditText) findViewById(R.id.add_name);
        add_passwrd = (EditText) findViewById(R.id.add_passwrd);
        add_address = (EditText) findViewById(R.id.add_address);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(add_name.getText().toString().isEmpty()){
                    add_name.setError("Required");
                    Toast.makeText(getApplicationContext(),"Enter UserName",Toast.LENGTH_SHORT).show();
                }else{

                    if(add_passwrd.getText().toString().isEmpty()){
                        add_passwrd.setError("Required");
                        Toast.makeText(getApplicationContext(),"Enter Password",Toast.LENGTH_SHORT).show();
                    }else{
                        if(add_address.getText().toString().isEmpty()){

                            add_address.setError("Required");
                            Toast.makeText(getApplicationContext(),"Enter Address",Toast.LENGTH_SHORT).show();
                        }else{

                            PrefUtils.SaveInfo(getApplicationContext(),add_name.getText().toString(),add_passwrd.getText().toString(),add_address.getText().toString());
                            Toast.makeText(getApplicationContext(),"Account Created",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    }
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
