package com.laskush.MLYD.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.laskush.MLYD.R;
import com.laskush.MLYD.adapter.PlaceAdapter;

public class About extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View content = inflater.inflate(R.layout.fragment_about, container, false);
        return  content;

    }
}
