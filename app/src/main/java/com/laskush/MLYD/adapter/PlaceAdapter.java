package com.laskush.MLYD.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.laskush.MLYD.R;


public class PlaceAdapter extends ArrayAdapter<String> {

private final Activity context;
private final String[] title;
        private final Integer[] imageId;

public PlaceAdapter(Activity context,
        String[] title, Integer[] imageId) {
        super(context, R.layout.activity_place_adapter, title);
        this.context = context;
        this.title = title;
        this.imageId = imageId;
}
@Override
public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.activity_place_adapter, null, true);
        TextView  recomendedPlace=(TextView)rowView.findViewById(R.id.recomendedPlaceList);
        recomendedPlace.setText(title[position]);

        return rowView;
        }
}
