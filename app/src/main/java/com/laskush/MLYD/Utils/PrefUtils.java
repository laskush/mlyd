package com.laskush.MLYD.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtils {

    public static final String SHARED_PREF_MLYD = "MLYD";

    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String ADDRESS = "address";

    public static void SaveInfo(Context context, String name, String password, String address) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_MLYD, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(NAME, name);
        editor.putString(PASSWORD, password);
        editor.putString(ADDRESS, address);
        editor.commit();

    }

    public static String Ret_Name(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_MLYD, Context.MODE_PRIVATE);
        return prefs.getString(NAME, "");
    }

    public static String Ret_Address(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_MLYD, Context.MODE_PRIVATE);
        return prefs.getString(ADDRESS, "");
    }

    public static String Ret_Password(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREF_MLYD, Context.MODE_PRIVATE);
        return prefs.getString(PASSWORD, "");
    }


}
